module.exports = {
    title: 'MetaLog',
    description: 'Информация по всем проектам METADATA',
    base: '/',
    dest: 'public',
    head: [
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&subset=cyrillic' }],
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&subset=cyrillic' }],
        ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Fira+Code:400,700&subset=cyrillic' }]
    ],
    themeConfig: {
        editLinks: false,
        nav: [],
        sidebar: {
            '/': [
                {
                    title: 'Введение',
                    collapsable: false,
                    children: [
                        '/team',
                        '/about'
                    ]
                },
                {
                    title: 'Проекты',
                    collapsable: false,
                    children: [
                        '/project/metadata',
                        '/project/unix',
                        '/project/webmasters',
                        '/project/furry',
                        '/project/radio'
                    ]
                },
                {
                    title: 'Правила',
                    collapsable: false,
                    children: [
                        '/rule/community',
                        '/rule/chat',
                        '/rule/coc',
                        '/rule/contributing',
                        '/rule/manifest'
                    ]
                }
            ]
        }
    }
};