# Фонд UNIX / Linux

- [UNIX Foundation](https://unix.foundation/) - фонд поддержки сообщества системных администраторов.
  - [UNIX Community](https://unix.community/) - форум сообщества системных администраторов.
  - [UNIX Chat](https://unixchat.app/)