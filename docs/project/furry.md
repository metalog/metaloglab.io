# Сообщество FURRY / ФУРРИ

- [FURRY Foundation](https://furry.foundation/) - фонд поддержки сообщества FURRY.
  - [FURRY Community](https://furry.community/) - форум сообщества FURRY.
  - [FURRY Chat](https://furry.chat/) - чат сообщества FURRY.